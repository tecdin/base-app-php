# Aplicación base para PHP
Este repositorio aloja la plantilla de aplicación base para proyectos desarrollados en PHP con las siguientes características:

* Apache
* MySQL
* PHP
* Yii2 Framework (Basic Template App)
* Bootstrap
* JQuery
* Morris Chart

## Última version estable
Aún no existe ninguna versión para producción

## Sobre la aplicación, información y otros 
La información, documentación y herramientas sobre el desarrollo de la aplicación se encuentran dentro de la carpeta /tecdin/.

## Información de contácto
[![Logo Tecdin](https://bitbucket.org/tecdin/base-app-php/raw/master/tecdin/logo-tecdin-166x66.png https://tecdin.com)](https://tecdin.com)

* Web: [tecdin.com](https://tecdin.com)
* Email: [desarrollo@tecdin.com](mailto:desarrollo@tecdin.com)
